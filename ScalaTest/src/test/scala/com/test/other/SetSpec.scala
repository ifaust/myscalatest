package com.test.other

class SetSpec extends UnitSpec {

  "An empty Set" should "have size 0" in {
    assert(Set.empty.size == 0)
  }

  it should "throw NoSuchElementException when head of an empty set is invoked" in {
    assertThrows[NoSuchElementException] { // Result type: Assertion
      Set.empty.head
    }
  }
}