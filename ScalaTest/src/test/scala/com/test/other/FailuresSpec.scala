package com.test.other

class FailuresSpec extends UnitSpec {
  val left = 2
  val right = 1
  //assert(left == right)

  val attempted = 2
  //assert(attempted == 1, "Execution was attempted " + left + " times instead of 1 time")

  //fail("I've got a bad feeling about this")

  //succeed // Has type Assertion

  val s = "hi"
  val caught =
    intercept[IndexOutOfBoundsException] { // Result type: IndexOutOfBoundsException
      s.charAt(-1)
    }
  assert(caught.getMessage.indexOf("-1") != -1)

  assertDoesNotCompile("val a: String = 1")

  assertTypeError("val a: String = 1") //OK
  //assertTypeError("val a: String = \"jh\"") //fail

  assertCompiles("val a: Int = 1")

  //cancel("Can't run the test because no internet connection was found")


  //Getting a clue
  //assert(1 + 1 === 3, "this is a clue")
  //assertResult(3, "this is a clue") { 1 + 1 }

  withClue("this is a clue") {
    assertThrows[IndexOutOfBoundsException] {
      "hi".charAt(-1)
    }
  }

  "A list" should "contain nonOf" in {
    List(1, 2, 3, 4, 5) should contain noneOf(7, 8, 9)
  }
  Some(0) should contain noneOf(7, 8, 9)
  "12345" should contain noneOf('7', '8', '9')
}
