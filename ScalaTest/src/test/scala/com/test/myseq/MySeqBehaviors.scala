package com.test.myseq

import org.scalatest.FlatSpec
import myseq.ListUtils.MySeq

trait MySeqBehaviors {
  this: FlatSpec =>

  def nonEmptyMySeq(newMySeq: => List[Int]) {

    it should "be non-empty" in {
      assert(newMySeq.nonEmpty)
    }

    it should "return non-zero if is empty" in {
      assert(newMySeq.size !== 0)
    }

  }

  def emptyMySeq(newMySeq: => List[Int]) {

    it should "not empty" in {
      assert(newMySeq.isEmpty)
    }

    it should "have zero size" in {
      assert(newMySeq.size === 0)
    }

    it should "return zero sign count" in {
      assertResult(0) {
        newMySeq.countSign
      }
    }
  }

}
