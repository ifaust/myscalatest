package com.test.myseq

import myseq.ListUtils.MySeq
import org.scalatest.FlatSpec

import scala.util.Random

class MySeqTest extends FlatSpec with MySeqBehaviors {

  // Stack fixture creation methods
  def emptySeq = List[Int]()

  def nonEmptySeq = {
    val n = scala.util.Random.nextInt(20) + 1
    List.fill(n)(Random.nextInt)
  }

  def seqWithOneItem = List[Int](Random.nextInt())

  "A MySeq (when empty)" should "be empty" in {
    assert(emptySeq.isEmpty)
  }

  it should "return zero" in {
    assertResult(0) {
      emptySeq.size
    }
  }

  it should behave like emptyMySeq(emptySeq)

  "A MySeq (with one item)" should behave like nonEmptyMySeq(seqWithOneItem)

  "A MySeq (non-empty)" should behave like nonEmptyMySeq(nonEmptySeq)

  it should "return 5" in {
    val mySeq = List(-1, 0, 1, 2, -3, 4, 5, -6, -7, 0, -8, 0, 9)
    assert(mySeq.countSign === 5)
  }
}

