package myseq

object ListUtils {

  implicit class MySeq(list: List[Int]) {

    def countSign: Int = {
      def iterate(s: List[Int], acc: Int = 0): Int = s match {
        case Nil => acc
        case x :: y :: _ if x * y < 0 => iterate(s.tail, acc + 1)
        case _ => iterate(s.tail, acc)
      }
      val filteredSeq = list filter (_ != 0)
      iterate(filteredSeq)
    }

  }

}
