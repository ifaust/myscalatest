import scala.util.Random
import myseq.ListUtils.MySeq

object Main extends App {
  val n = scala.util.Random.nextInt(20) + 1
  val a = List.fill(n)(Random.nextInt(201) - 100)

  println(a)
  println(s"countSign = ${a.countSign}")
}
